define([
	'require',
	'madjoh_modules/ajax/ajax',
],
function(require, AJAX){
	var GameGift = {
		send : function(friend_ids, code, data, rejectable){
			var parameters = {
				friend_ids 	: JSON.stringify(friend_ids),
				code 		: code
			};

			if(rejectable) 	parameters.rejectable 	= rejectable;
			if(data) 		parameters.data 		= JSON.stringify(data);

			return AJAX.post('/gift/send', {parameters : parameters});
		},
		answer : function(game_gift_id, accept){
			var parameters = {
				game_request_id : game_request_id,
				accept 			: accept
			};

			return AJAX.post('/gift/answer', {parameters : parameters});
		},

		getReceived : function(){
			return AJAX.post('/gift/get/received');
		},
		getSent : function(){
			return AJAX.post('/gift/get/sent');
		}
	};

	return GameGift;
});